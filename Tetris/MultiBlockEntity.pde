
public class MultiBlockEntity {

  private ArrayList<BlockEntity> blocks;

  public MultiBlockEntity(ArrayList<BlockEntity> blocks) {
    this.blocks = blocks;
  }

  public boolean canMove(int deltaCol, int deltaRow, BlockContext blockContext) {
    for (int i = 0; i < blocks.size(); i++)
      if (!blocks.get(i).canMove(deltaCol, deltaRow, blockContext))
        return false;
    return true;
  }

  public void move(int deltaCol, int deltaRow) {
    for (BlockEntity block : blocks)
      block.move(deltaCol, deltaRow);
  }

  public void rotate(BlockContext blockContext) {
    // deep-copy to a new list
    ArrayList<BlockEntity> newBlocks = new ArrayList<BlockEntity>();

    // translate 0,0 to the center of the multiBlock
    int colTranslate = minCol() + width() / 2;
    int rowTranslate = minRow() + height() / 2;
    for (BlockEntity block : blocks) {
      int oldCol = block.col - colTranslate; // the old block pos as seen from 0,0 as center
      int oldRow = block.row - rowTranslate;
      int newCol = -oldRow; // actual rotation around 0,0
      int newRow = oldCol;
      newCol += colTranslate; // translate back to old pos
      newRow += rowTranslate;
      if (blockContext.isBlockAt(newCol, newRow))
        return;
      newBlocks.add(new BlockEntity(newCol, newRow, block.c));
    }
    blocks = newBlocks;
  }

  public void show(int xOffset, int  yOffset, int size) {
    for (int i = 0; i < blocks.size(); i++)
      blocks.get(i).show(xOffset, yOffset, size);
  }

  public int minCol() {
    int minCol = Integer.MAX_VALUE;
    for (int i = 0; i < blocks.size(); i++)      
      if (blocks.get(i).col < minCol)
        minCol = blocks.get(i).col;

    return minCol;
  }

  public int minRow() {
    int minRow = Integer.MAX_VALUE;
    for (int i = 0; i < blocks.size(); i++)      
      if (blocks.get(i).row < minRow)
        minRow = blocks.get(i).row;
    return minRow;
  }

  public int maxCol() {
    int maxCol = Integer.MIN_VALUE;
    for (int i = 0; i < blocks.size(); i++)      
      if (blocks.get(i).col > maxCol)
        maxCol = blocks.get(i).col;
    return maxCol;
  }

  public int maxRow() {
    int maxRow = Integer.MIN_VALUE;
    for (int i = 0; i < blocks.size(); i++) 
      if (blocks.get(i).row > maxRow)
        maxRow = blocks.get(i).row;
    return maxRow;
  }

  public int width() {
    return maxCol() - minCol();
  }

  public int height() {
    return maxRow() - minRow();
  }
}