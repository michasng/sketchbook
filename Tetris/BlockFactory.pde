public class BlockFactory {

  /**
   * @return random "Tetrominos"
   */
  public MultiBlockEntity generate() {
    switch((int)random(7)) {
    case 0:
      return I();
    case 1:
      return O();
    case 2:
      return T();
    case 3:
      return J();
    case 4:
      return L();
    case 5:
      return S();
    case 6:
      return Z();
    }
    throw new Error("Could not generate block");
  }

  public MultiBlockEntity I() {
    color c = #05F7E1;
    ArrayList<BlockEntity> blocks = new ArrayList<BlockEntity>();
    blocks.add(new BlockEntity(0, 0, c));
    blocks.add(new BlockEntity(0, 1, c));
    blocks.add(new BlockEntity(0, 2, c));
    blocks.add(new BlockEntity(0, 3, c));
    return new MultiBlockEntity(blocks);
  }

  public MultiBlockEntity O() {
    color c = #0300FF;
    ArrayList<BlockEntity> blocks = new ArrayList<BlockEntity>();
    blocks.add(new BlockEntity(0, 0, c));
    blocks.add(new BlockEntity(1, 0, c));
    blocks.add(new BlockEntity(0, 1, c));
    blocks.add(new BlockEntity(1, 1, c));
    return new MultiBlockEntity(blocks);
  }

  public MultiBlockEntity T() {
    color c = #FFAF00;
    ArrayList<BlockEntity> blocks = new ArrayList<BlockEntity>();
    blocks.add(new BlockEntity(0, 0, c));
    blocks.add(new BlockEntity(1, 0, c));
    blocks.add(new BlockEntity(2, 0, c));
    blocks.add(new BlockEntity(1, 1, c));
    return new MultiBlockEntity(blocks);
  }

  public MultiBlockEntity J() {
    color c = #FFF700;
    ArrayList<BlockEntity> blocks = new ArrayList<BlockEntity>();
    blocks.add(new BlockEntity(1, 0, c));
    blocks.add(new BlockEntity(1, 1, c));
    blocks.add(new BlockEntity(1, 2, c));
    blocks.add(new BlockEntity(0, 2, c));
    return new MultiBlockEntity(blocks);
  }

  public MultiBlockEntity L() {
    color c = #03FF00;
    ArrayList<BlockEntity> blocks = new ArrayList<BlockEntity>();
    blocks.add(new BlockEntity(0, 0, c));
    blocks.add(new BlockEntity(0, 1, c));
    blocks.add(new BlockEntity(0, 2, c));
    blocks.add(new BlockEntity(1, 2, c));
    return new MultiBlockEntity(blocks);
  }

  public MultiBlockEntity S() {
    color c = #6200C6;
    ArrayList<BlockEntity> blocks = new ArrayList<BlockEntity>();
    blocks.add(new BlockEntity(0, 1, c));
    blocks.add(new BlockEntity(1, 1, c));
    blocks.add(new BlockEntity(1, 0, c));
    blocks.add(new BlockEntity(2, 0, c));
    return new MultiBlockEntity(blocks);
  }

  public MultiBlockEntity Z() {
    color c = #F70000;
    ArrayList<BlockEntity> blocks = new ArrayList<BlockEntity>();
    blocks.add(new BlockEntity(0, 0, c));
    blocks.add(new BlockEntity(1, 0, c));
    blocks.add(new BlockEntity(1, 1, c));
    blocks.add(new BlockEntity(2, 1, c));
    return new MultiBlockEntity(blocks);
  }
}