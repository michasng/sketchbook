public static final int FALL_DELAY = 15;

private int fallTimer;
private boolean pressedKey;
private int score;
private boolean gameOver;

private int numCols, numRows, blockSize;
private int xOffset, yOffset;

private color[][] blocks;
private MultiBlockEntity controlledBlock;
private ArrayList<MultiBlockEntity> fallingBlocks;
private BlockContext blockContext;

void setup() {
  size(1024, 768);
  // fullScreen();
  frame.toFront();
  frame.requestFocus();
  frameRate(30);

  numCols = 8;
  numRows = 16;
  if (width / numCols < height / numRows)
    blockSize = width / numCols;
  else
    blockSize = height / numRows;
  xOffset = (width - blockSize * numCols) / 2;
  yOffset = (height - blockSize * numRows) / 2;

  blocks = new color[numRows][numCols];
  fallingBlocks = new ArrayList<MultiBlockEntity>();
  blockContext = new BlockContext();
}

private void playerInput() {
  if (controlledBlock != null && !gameOver && key==CODED) { // the key is not just a char or int
    if (keyCode == LEFT && controlledBlock.canMove(-1, 0, blockContext))
      controlledBlock.move(-1, 0);
    else if (keyCode == RIGHT && controlledBlock.canMove(1, 0, blockContext))
      controlledBlock.move(1, 0);
    else if (keyCode == DOWN && controlledBlock.canMove(0, 1, blockContext))
      controlledBlock.move(0, 1);
    else if (keyCode == UP)
      controlledBlock.rotate(blockContext);
  } else if (key == 'r') {
    blocks = new color[numRows][numCols];
    controlledBlock = null;
    fallingBlocks = new ArrayList<MultiBlockEntity>();
    gameOver = false;
    score = 0;
  }

  pressedKey = false;
}

private boolean dropBlocks() {
  boolean anyFalling = false;
  ArrayList<MultiBlockEntity> toRemove = new ArrayList<MultiBlockEntity>();
  for (MultiBlockEntity entity : fallingBlocks) {
    if (entity.canMove(0, 1, blockContext)) {
      entity.move(0, 1);
      anyFalling = true;
    } else {
      addBlocks(entity.blocks);
      toRemove.add(entity);
    }
  }
  fallingBlocks.removeAll(toRemove);
  return anyFalling;
}

/**
 * Adds all blocks from an ArrayList to the field
 * @return true, if all blocks were within the fields bounds, false at least one wasn't
 */
private boolean addBlocks(ArrayList<BlockEntity> newBlocks) {
  boolean addedAll = true;
  for (BlockEntity newBlock : newBlocks)
    if (newBlock.row >= 0 && newBlock.row < numRows && newBlock.col >= 0 && newBlock.col < numCols)
      this.blocks[newBlock.row][newBlock.col] = newBlock.c;
    else addedAll = false;
  return addedAll;
}

private void addControlledBlocks() {
  if (!addBlocks(controlledBlock.blocks))
    gameOver = true;
  controlledBlock = null;
  int numFullRows = checkRows();
  if (numFullRows > 0)
    score+= numFullRows;
}

private int checkRows() {
  int numFullRows = 0;
  for (int row = 0; row < numRows; row++) {
    int rowBlockCount = 0;
    for (int col = 0; col < numCols; col++)
      if (blocks[row][col] != 0)
        rowBlockCount++;

    if (rowBlockCount == numCols) {
      deleteRow(row);
      numFullRows++;
    }
  }
  return numFullRows;
}

private void deleteRow(int deleteRow) {
  ArrayList<BlockEntity> blocksAbove = new ArrayList<BlockEntity>();
  for (int col = 0; col < numCols; col++) {
    blocks[deleteRow][col] = 0; // delete all blocks in the deleteRow
    for (int row = 0; row < deleteRow; row++) // and add all blocks above to an ArrayList
      if (blocks[row][col] != 0) {
        blocksAbove.add(new BlockEntity(col, row, blocks[row][col]));
        blocks[row][col] = 0;
      }
  }

  if (blocksAbove.size() > 0)
    fallingBlocks.add(new MultiBlockEntity(blocksAbove));
}

private void createBlock() {
  controlledBlock = new BlockFactory().generate();
  int col = (int)random(numCols - controlledBlock.width());
  int row = -controlledBlock.height();
  controlledBlock.move(col, row);
}

void draw() {

  if (pressedKey) {
    playerInput();
  }

  fallTimer++;
  if (fallTimer > FALL_DELAY && !gameOver) {
    fallTimer = 0;

    if (!dropBlocks())
      if (controlledBlock == null)
        createBlock();
      else if (controlledBlock.canMove(0, 1, blockContext)) {
        controlledBlock.move(0, 1);
      } else {
        addControlledBlocks();
        controlledBlock = null;
      }
  }


  background(100);
  for (int row = 0; row < numRows; row++)
    for (int col = 0; col < numCols; col++) {
      fill(blocks[row][col]);
      rect(col * blockSize + xOffset, row * blockSize + yOffset, blockSize, blockSize);
    }
  for (MultiBlockEntity entity : fallingBlocks)
    entity.show(xOffset, yOffset, blockSize);
  if (controlledBlock != null)
    controlledBlock.show(xOffset, yOffset, blockSize);
  fill(255); // white
  textSize(height / 8); // textSize in pixels
  textAlign(RIGHT, TOP); // first x, then y alignment
  text(score, width, 0);
  if (gameOver) {
    textAlign(CENTER, CENTER);
    text("Game Over", width / 2, height / 2);
  }
}

void keyPressed() {
  pressedKey = true; // keyPressed-keyword is already used by Processing
}

void keyReleased() {
  pressedKey = false;
}

public color getBlock(int col, int row) {
  if (row < 0 || row >= numRows || col < 0 || col >= numCols)
    return 0;
  else
    return blocks[row][col];
}

private class BlockContext {

  public boolean isBlockAt(int col, int row) {
    // check if out of bottom/left/right bounds
    if (row >= numRows || col < 0 || col >= numCols) return true;
    if (row < 0) // top counts as within bounds, bc blockEntities can move there
      return false;
    return blocks[row][col] != 0;
  }
}