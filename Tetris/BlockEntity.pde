public class BlockEntity {

  protected int col, row;
  protected color c;

  public BlockEntity(int col, int row, int c) {
    this.col = col;
    this.row = row;
    this.c = c;
  }

  public boolean canMove(int deltaCol, int deltaRow, BlockContext blockContext) {
    return (!blockContext.isBlockAt(col + deltaCol, row + deltaRow));
  }

  public void move(int deltaCol, int deltaRow) {
    this.col += deltaCol;
    this.row += deltaRow;
  }

  public void show(int xOffset, int  yOffset, int size) {
    if (row < 0) // don't show blocks above the map
      return;
    fill(c);
    rect(col * size + xOffset, row * size + yOffset, size, size);
    // ellipseMode(CORNER);
    // ellipse(col * size + xOffset, row * size + yOffset, size, size);
  }
}