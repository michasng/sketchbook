public class Paddle extends Entity {

  private int w, h;
  private float friction; // slows down over time

  public Paddle(float x, float y, int w, int h) {
    super(x, y);
    this.w = w;
    this.h = h;
    friction = 0.5;
  }

  // auto center vertically
  public Paddle(float x, int w, int h) {
    this(x, height / 2 - h/2, w, h);
  }

  @Override
    public void calcVel() {
    super.calcVel();
    vel.mult(friction);
  }

  @Override
    public void calcNextPos() {
    super.calcNextPos();
    if (nextPos.y < 0)
      nextPos.y = 0;
    if (nextPos.y + h >= height)
      nextPos.y = height - h;
  }

  @Override
    public void checkCollision(Puck puck) {
    if (Collider.rectCir(nextPos, w, h, puck.pos, puck.radius))
      puck.deflect(this);
  }

  @Override
    public void checkCollision(Paddle paddle) {
    if (Collider.rectRect(nextPos, w, h, paddle.pos, paddle.w, paddle.h))
      nextPos = new PVector(pos.x, pos.y);
  }

  @Override
    public void show() {
    rect(pos.x, pos.y, w, h);
  }
}