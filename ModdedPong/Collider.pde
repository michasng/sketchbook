public static class Collider {

  // https://stackoverflow.com/questions/401847/circle-rectangle-collision-detection-intersection
  public static boolean rectCir(PVector recPos, int recW, int recH, PVector cirPos, int radius) {
    PVector recCenter = new PVector(recPos.x + recW / 2, recPos.y + recH / 2);
    float xDist = abs(cirPos.x - recCenter.x);
    float yDist = abs(cirPos.y - recCenter.y);

    if (xDist > (recW/2 + radius)) { 
      return false;
    }
    if (yDist > (recH/2 + radius)) { 
      return false;
    }

    if (xDist <= (recW/2)) { 
      return true;
    } 
    if (yDist <= (recH/2)) { 
      return true;
    }
    
    double cornerDistance_sq = Math.pow(xDist - recW/2, 2) + Math.pow(yDist - recH/2, 2);

    return (cornerDistance_sq <= Math.pow(radius, 2));
  }

  public static boolean rectRect(PVector r1Pos, int r1W, int r1H, PVector r2Pos, int r2W, int r2H) {
    return r1Pos.x + r1W >= r2Pos.x && r2Pos.x + r2W >= r1Pos.x && r1Pos.y + r1H >= r2Pos.y && r2Pos.y + r2H >= r1Pos.y;
  }

  public static boolean cirCir(PVector c1Pos, int c1R, PVector c2Pos, int c2R) {
    return c1Pos.dist(c2Pos) <= c1R + c2R;
  }
}