public abstract class Entity {

  protected PVector pos, nextPos, vel, acc;
  protected int maxVel; // limits the velocity

  public Entity(float x, float y) {
    this(x, y, new PVector());
  }

  public Entity(float x, float y, PVector vel) {
    this.pos = new PVector(x, y);
    this.vel = vel;
    this.acc = new PVector();
    this.maxVel = height / 36;
  }

  public void calcVel() {
    vel.add(acc);
    vel.limit(maxVel);
    acc.mult(0);
  }

  public void calcNextPos() {
    nextPos = new PVector(pos.x, pos.y);
    nextPos.add(vel);
  }

  public void collide(ArrayList<Entity> entities) {
    for (Entity entity : entities) {
      if (entity == this)
        continue;
      if (entity instanceof Puck) {
        checkCollision((Puck)entity);
      } else if (entity instanceof Paddle) {
        checkCollision((Paddle)entity);
      } else throw new Error("Unknown entity type");
    }
  }

  public abstract void checkCollision(Paddle paddle);
  public abstract void checkCollision(Puck puck);

  public void move() {
    pos = nextPos;
  }

  public void applyForce(int xForce, int yForce) {
    applyForce(new PVector(xForce, yForce));
  }

  public void applyForce(PVector force) {
    this.acc.add(force);
  }

  public abstract void show();
}