public class Puck extends Entity {

  private int radius;
  private int startX, startY;
  private float startSpeed, speedUp;

  // auto center
  public Puck(int x, int y, int radius) {
    super(x, y);
    this.startX = x;
    this.startY = y;
    this.radius = radius;
    this.startSpeed = radius / 3f;
    this.speedUp = radius / 24f; // float for precision
  }

  public void reset() {
    pos = new PVector(startX, startY);
    vel = PVector.random2D().mult(startSpeed);
  }

  @Override
    public void calcNextPos() {
    super.calcNextPos();

    if (nextPos.y-radius <= 0 || nextPos.y+radius > height)
      vel.y *= -1;

    super.calcNextPos();
  }

  @Override
    public void checkCollision(Paddle paddle) {
    if (Collider.rectCir(paddle.pos, paddle.w, paddle.h, nextPos, radius)) {
      deflect(paddle);
    }
  }

  @Override
    public void checkCollision(Puck puck) {
    if (Collider.cirCir(nextPos, radius, puck.pos, puck.radius))
      deflect(puck.pos);
  }

  public void deflect(Paddle paddle) {
    deflect(new PVector(paddle.pos.x + paddle.w / 2, paddle.pos.y + paddle.h / 2));
    float currentVel = vel.mag();
    vel.normalize().mult(currentVel + speedUp); // only speed-up when hitting a paddle
  }

  public void deflect(PVector deflectPoint) {
    PVector dirToPuck = new PVector(pos.x, pos.y).sub(deflectPoint).normalize();
    vel = dirToPuck.mult(vel.mag());
    calcNextPos();
  }

  @Override
    public void show() {
    ellipseMode(CENTER);
    ellipse(pos.x, pos.y, 2 * radius, 2 * radius);
  }
}