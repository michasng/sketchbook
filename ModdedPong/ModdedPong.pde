private IntList keyList;

private ArrayList<Entity> entities;
private Paddle leftPaddle, rightPaddle;

private int scoreLeft, scoreRight;

boolean gravity, autoControlLeft, autoControlRight;

public void setup() {
  // size(1024, 768);
  fullScreen();
  frame.toFront();
  frame.requestFocus(); // window accepts key events right away
  frameRate(30);

  keyList = new IntList();

  entities = new ArrayList<Entity>();
  int radius = height / 32;
  // entities.add(new Puck(width / 2, height / 2, radius));
  entities.add(new Puck(width / 2 - radius, height / 2 - radius, radius));
  entities.add(new Puck(width / 2 + radius, height / 2 + radius, radius));
  entities.add(new Puck(width / 2 + radius, height / 2 - radius, radius));
  entities.add(new Puck(width / 2 - radius, height / 2 + radius, radius));
  leftPaddle = new Paddle(radius, radius, radius * 4);
  entities.add(leftPaddle);
  rightPaddle = new Paddle(width - 2 * radius, radius, radius * 4); 
  entities.add(rightPaddle);
}

public void updateEntity(Entity entity) {
  entity.calcVel();
  if (gravity && entity instanceof Puck)
    entity.applyForce(0, height / 1024);
  entity.calcNextPos();
  entity.collide(entities);
  entity.move();
  entity.show();
}

public void autoControl(Paddle paddle) {
  // find closest puck
  float minDistance = Integer.MAX_VALUE;
  Entity closestPuck = null;
  for (Entity entity : entities)
    if (entity instanceof Puck) {
      float currentDistance = entity.pos.dist(paddle.pos);
      if (currentDistance < minDistance) {
        minDistance = currentDistance;
        closestPuck = entity;
      }
    }

  int paddleAcc = height / 8;
  if (closestPuck.pos.y < paddle.pos.y)
    paddle.applyForce(0, -paddleAcc);
  else if (closestPuck.pos.y > paddle.pos.y + paddle.h)
    paddle.applyForce(0, paddleAcc);
}

public void draw() {
  background(0);

  if (keyList.size() > 0)
    onKeyPressed();

  if (autoControlLeft)
    autoControl(leftPaddle);
  if (autoControlRight)
    autoControl(rightPaddle);

  for (Entity entity : entities)
    updateEntity(entity);

  checkGoals();

  textSize(height / 10);
  textAlign(RIGHT, TOP);
  text(scoreLeft + ":", width / 2, 0);
  textAlign(LEFT, TOP);
  text(scoreRight, width / 2, 0);

  // textAlign(LEFT, TOP);
  // text(frameRate, 0, 0);
}

public void checkGoals() {
  for (Entity entity : entities) {
    if (entity instanceof Puck) {
      Puck puck = (Puck)entity;
      if (puck.pos.x - puck.radius <= 0) {
        scoreRight++;
        puck.reset();
      }

      if (puck.pos.x + puck.radius > width) {
        scoreLeft++;
        puck.reset();
      }
    }
  }
}

public void onKeyPressed() {
  int paddleAcc = height / 8;

  for (int currentKey : keyList)
    switch(currentKey) {
    case 'w':
      leftPaddle.applyForce(0, -paddleAcc);
      break;
    case 's':
      leftPaddle.applyForce(0, paddleAcc);
      break;
    case UP:
      rightPaddle.applyForce(0, -paddleAcc);
      break;
    case DOWN:
      rightPaddle.applyForce(0, paddleAcc);
      break;
    case ' ':
      for (Entity entity : entities)
        if (entity instanceof Puck)
          ((Puck)entity).reset();
      keyList.removeValues(' ');
      break;
    case 'r':
      for (Entity entity : entities)
        if (entity instanceof Puck)
          entity.applyForce(PVector.random2D());
      break;
    case LEFT:
      autoControlLeft = !autoControlLeft;
      keyList.removeValue(LEFT);
      break;
    case RIGHT:
      autoControlRight = !autoControlRight;
      keyList.removeValues(RIGHT);
      break;
    case 'g':
      gravity = !gravity;
      keyList.removeValues('g');
      break;
    }
}

public void keyPressed() {
  keyList.append(key == CODED ? keyCode: key);
}

public void keyReleased() {
  keyList.removeValues(key == CODED ? keyCode : key);
}