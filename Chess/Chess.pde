private int tileSize;
private int xOffset;
private Figure[][] figures;
private Figure selectedFigure;

private boolean clickedField;
private int clickedCol, clickedRow; 

private PImage spriteSheet;

void setup() {
  size(1024, 768);
  frame.toFront();
  frame.requestFocus();
  frameRate(30);
  noSmooth(); // pixelated scaling

  tileSize = height / 8;
  xOffset = width / 2 - (8 * tileSize) / 2;

  spriteSheet = loadImage("chessFigures.png");
  int imgSize = spriteSheet.height / 2;
  figures = new Figure[8][8];
  figures[7][0] = new Tower(true, getSubImage(48, 0, imgSize));
  figures[6][0] = new Pawn(true, getSubImage(0, 0, imgSize));
  figures[6][1] = new Pawn(true, getSubImage(0, 0, imgSize));
}

public void mouseClicked() {
  if (pmouseX < xOffset || pmouseX >= xOffset + 8 * tileSize) {
    clickedField = false;
    if (selectedFigure != null)
      selectedFigure.setSelected(false);
    selectedFigure = null;
    return;
  }

  clickedField = true;
  int x = mouseX - xOffset;
  int y = mouseY;
  clickedCol = x / tileSize;
  clickedRow = y / tileSize;

  if (selectedFigure == null && figures[clickedRow][clickedCol]!= null) {
    figures[clickedRow][clickedCol].setSelected(true);
    selectedFigure = figures[clickedRow][clickedCol];
  }
}

void draw() {
  background(#d2d2d2);

  for (int row = 0; row < 8; row++)
    for (int col = 0; col < 8; col++)
    {
      if (row % 2 == 0 ^ col % 2 == 0)
        fill(100);
      else
        fill(200);
      rect(col * tileSize + xOffset, row * tileSize, tileSize, tileSize);
    }

  for (int row = 0; row < 8; row++)
    for (int col = 0; col < 8; col++) {
      if (figures[row][col] == null)
        continue;
      figures[row][col].show(col * tileSize + xOffset, row * tileSize, tileSize);
      // figures[row][col].show(col * tileSize + xOffset, row * tileSize, tileSize);
    }

  if (clickedField) {
    fill(color(0, 255, 0, 100));
    rect(clickedCol * tileSize + xOffset, clickedRow * tileSize, tileSize, tileSize);
  }
}

public PImage getSubImage(int imgX, int imgY, int imgSize) {
  spriteSheet.loadPixels(); // loads the pixel data into the pixels array
  PImage result = createImage(imgSize, imgSize, ARGB);
  result.blend(spriteSheet,imgX,imgY,imgSize,imgSize,0,0,imgSize,imgSize,BLEND);
  /**
  result.loadPixels();
  for (int y = 0; y < imgSize; y++)
    for (int x = 0; x < imgSize; x++) {
      int resultIndex = y * result.width + x;
      int sheetIndex = (y + imgY) * spriteSheet.width + x + imgX;
      result.pixels[resultIndex] = spriteSheet.pixels[sheetIndex];
    }
  result.updatePixels(); // updates the image from it's pixel array
  */
  return result;
}