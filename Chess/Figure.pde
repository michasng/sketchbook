public abstract class Figure {

  protected boolean whiteSide;

  private PImage image;  
  private boolean selected;

  public Figure(boolean whiteSide, PImage image) {
    this.whiteSide = whiteSide;
    this.image = image;
  }

  // public abstract boolean canMoveTo(int targetCol, int targetRow);

  public void show(int x, int y, int tileSize) {
    image(image, x, y, tileSize, tileSize);
  }

  public void setSelected(boolean selected) {
    this.selected = selected;
  }
}