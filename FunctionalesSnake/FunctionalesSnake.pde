import java.util.LinkedList;

public static final int MOVE_DELAY = 5;

private int moveTimer;
private IntList pressedKeys, releasedKeys;
private boolean gameOver;

private int numCols, numRows, tileSize;
private int xOffset, yOffset;

private boolean[][] grid;
private LinkedList<PVector> snake;
private PVector currentMoveDir;
private PVector food;

void setup() {
  size(1024, 768);
  // fullScreen(); 
  frame.toFront();
  frame.requestFocus();
  frameRate(30);
  pressedKeys = new IntList();
  releasedKeys = new IntList();

  numCols = 16;
  numRows = 16;
  if (width / numCols < height / numRows)
    tileSize = width / numCols;
  else
    tileSize = height / numRows;
  xOffset = (width - tileSize * numCols) / 2;
  yOffset = (height - tileSize * numRows) / 2;

  newGame();
}

public void newGame() {
  grid = new boolean[numRows][numCols];
  snake = new LinkedList<PVector>();
  snake.push(new PVector(numCols / 2, numRows / 2));
  grid[numRows / 2][numCols / 2] = true;
  currentMoveDir = new PVector(0, 0);
  createFood();
  gameOver = false;
}

public void createFood() {
  int randomCol, randomRow;
  do {
    randomCol = (int)random(numCols);
    randomRow = (int)random(numRows);
  } while (grid[randomRow][randomCol]);

  food = new PVector(randomCol, randomRow);
  grid[randomRow][randomCol] = true;
}

public void eatFood() {
  if (snake.size() == numCols * numRows)
    gameOver = true; // winning the game

  createFood();
}

public boolean outOfBounds(PVector pt) {
  return pt.x < 0 || pt.x >= numCols || pt.y < 0 || pt.y >= numRows;
}

public void move(int moveX, int moveY) {
  if (moveX == 0 && moveY == 0)
    return;
  currentMoveDir = new PVector(moveX, moveY);

  PVector oldTop = snake.peekFirst();
  PVector newTop = oldTop.copy().add(moveX, moveY);
  if (food.equals(newTop))
    eatFood();
  else if (outOfBounds(newTop) || grid[(int)newTop.y][(int)newTop.x] == true) {
    gameOver = true;
    return;
  } else { // only remove last, when no food was eaten
    PVector last = snake.removeLast();  
    grid[(int)last.y][(int)last.x] = false;
  }
  snake.addFirst(newTop);
  grid[(int)newTop.y][(int)newTop.x] = true;
}

void draw() {
  moveTimer++;
  if (moveTimer > MOVE_DELAY && !gameOver) {
    moveTimer = 0;

    if (pressedKeys.size() > 0)
      onKeyPressed();
    else move((int)currentMoveDir.x, (int)currentMoveDir.y);
  }

  background(0); // draw black background

  noStroke(); // don't show outlines
  fill(150); // draw map
  rect(xOffset, yOffset, numCols * tileSize, numRows * tileSize);

  fill(#00ff00); // draw snake
  ellipseMode(CORNER);
  for (PVector element : snake)
    ellipse(element.x * tileSize + xOffset, element.y * tileSize + yOffset, tileSize, tileSize);

  fill(#ff0000); // draw food
  ellipse(food.x * tileSize + xOffset, food.y * tileSize + yOffset, tileSize, tileSize);

  if (gameOver) { // draw gameOver message
    fill(255);
    textSize(height / 8);
    textAlign(CENTER, CENTER);
    text("Game Over", width / 2, height / 2);
  }
}

public void onKeyPressed() {
  // only allow 1 key at a time
  switch(pressedKeys.get(0)) {
  case UP:
  case 'w':
    move(0, -1);
    break;
  case RIGHT:
  case 'd':
    move(1, 0);
    break;
  case DOWN:
  case 's':
    move(0, 1);
    break;
  case LEFT:
  case 'a':
    move(-1, 0);
    break;
  }

  // remove all released keys
  IntList rKCopy = releasedKeys.copy(); // bc of changing indexes
  for (int code : rKCopy) {
    pressedKeys.removeValues(code);
    releasedKeys.removeValues(code);
  }
}

public void keyPressed() {
  int value = key == CODED ? keyCode: key;
  if (value == 'r')
    newGame();
  else if (!pressedKeys.hasValue(value))
    pressedKeys.append(value);
}

public void keyReleased() {
  int value = key == CODED ? keyCode: key;
  if (!releasedKeys.hasValue(value))
    releasedKeys.append(value);
}