private ParticleContext ctx;

void setup() {
  size(1024, 768);
  // fullScreen();

  ctx = new ParticleContext();
}

void keyPressed() {
  ctx.shootFirework();
}

void draw() {
  background(0);

  for (Particle p : ctx.particles) {
    p.update(ctx);
    p.show();
  }
  ctx.particles.removeAll(ctx.toRemove);
  ctx.toRemove.clear(); 
  ctx.particles.addAll(ctx.toAdd);
  ctx.toAdd.clear();
}

private class ParticleContext {

  private ArrayList<Particle> particles;
  private ArrayList<Particle> toRemove;
  private ArrayList<Particle> toAdd;

  public ParticleContext() {
    particles = new ArrayList<Particle>();
    toRemove = new ArrayList<Particle>();
    toAdd = new ArrayList<Particle>();
    for (int i = 0; i < 1; i++) {
      shootFirework();
    }
  }

  public void shootFirework() {
    addParticle(new Firework(map(random(1),0, 1, width/4, width - width/4), height, color(random(255), random(255), random(255))));
  }

  public void addParticle(Particle p) {
    toAdd.add(p);
  }

  public void removeParticle(Particle p) {
    toRemove.add(p);
  }
}