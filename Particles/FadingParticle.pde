public class FadingParticle extends Particle {

  private int a;

  public FadingParticle(float x, float y, color c) {
    this(x, y, c, new PVector(0, 0));
  }

  public FadingParticle(float x, float y, color c, PVector vel) {
    super(x, y, c, vel);
    a = 255;
  }

  @Override
    public void update(ParticleContext ctx) {
    super.update(ctx);
    a -= 5;
    if (a <= 0)
      ctx.removeParticle(this);
  }

@Override
  public void show() {
    c = color(red(c), green(c), blue(c), a);
    super.show();
    // alpha(0);
  }
}