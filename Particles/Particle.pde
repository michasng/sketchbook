public class Particle {

  protected PVector pos, vel, acc;
  protected int w, h;
  protected color c;

  public Particle(float x, float y, color c) {
    this(x, y, c, new PVector(0, 0));
  }

  public Particle(float x, float y, color c, PVector vel) {
    pos = new PVector(x, y);
    this.vel = vel;
    acc = new PVector(0, 0);
    w = width / 100;
    h = width / 100;
    this.c = c;
  }

  public void applyForce(PVector force) {
    acc.add(force);
  }

  public void update(ParticleContext ctx) {
    vel.add(acc);
    acc.mult(0);
    vel.add(0, 0.5);
    vel.mult(0.9);
    pos.add(vel);
    
    if(pos.y > height)
      ctx.removeParticle(this);
  }

  public void show() {
    fill(c);
    rect(pos.x - w, pos.y - h, w, h);
  }
}