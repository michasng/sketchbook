public class Firework extends Particle {

  public Firework(float x, float y, color c) {
    super(x, y, c);

    applyForce(new PVector(0, -(height/16 + random(height/16))));
  }

  @Override
    public void update(ParticleContext ctx) {
    super.update(ctx);
    if (vel.y > 0) {
      for (int i = 0; i < 100; i++)
        ctx.addParticle(new FadingParticle(pos.x, pos.y, color(random(255), random(255), random(255)), PVector.random2D().mult(random(16))));
      ctx.removeParticle(this);
    }
  }
}