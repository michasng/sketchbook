class Tile {

  int size;
  
  Tile(int size) {
    this.size = size;
  }
                                                                       
  void show(int col, int row, boolean isSelected) {
    float a =  cos(radians(30)) * size;
    float g =  0.5 * size; // sin(radians(30)) = 0.5
    float x = col * 2 * a;
    float y = row * 1.5 * size;
    if(row % 2 == 1)
      x += a;
    stroke(0);
    if(isSelected)
      fill(0);
    else
      fill(255);
    beginShape();
    vertex(x + a, y);
    vertex(x + a + a, y + g);
    vertex(x + a + a, y + size + size - g);
    vertex(x + a, y + size + size);
    vertex(x, y + size + size - g);
    vertex(x, y + g);
    endShape(CLOSE);
  }  
  
}
