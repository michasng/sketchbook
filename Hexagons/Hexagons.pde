
Tile[][] map;
int tileSize;
final int numCols = 10;
final int numRows = 10;

int selX, selY;

boolean gridEnabled, hitboxesEnabled;

void setup() {
  size(1000, 1000);
  
  map = new Tile[numRows][numCols];
  tileSize = 50;
  for(int row = 0; row < map.length; row++) {
    for(int col = 0; col < map[row].length; col++) {
      map[row][col] = new Tile(tileSize);
    }
  }
  selX = numCols / 2;
  selY = numRows / 2;
    
}

void keyPressed() {
  switch(key) {
    case 'g':
      gridEnabled = !gridEnabled;
      break;
    case 'h':
      hitboxesEnabled = !hitboxesEnabled;
      break;
    case 'e':
      if(selY % 2 == 1)
        selX++;
      selY--;
      break;
    case 'd':
      selX++;
      break;
    case 'x':
      if(selY % 2 == 1)
        selX++;
      selY++;
      break;
    case 'y':
      if(selY % 2 == 0)
        selX--;
      selY++;
      break;
    case 'a':
      selX--;
      break;
    case 'w':
      if(selY % 2 == 0)
        selX--;
      selY--;
      break;
  }
}

void mouseClicked() {
  PVector tilePos = toMatrixPos(mouseX, mouseY);
  int col = (int)tilePos.x;
  int row = (int)tilePos.y;
  if(isOutOfBounds(col, row))
    return;
  selX = col;
  selY = row;
  
}

boolean isOutOfBounds(int col, int row) {
  return (col < 0 || col >= numCols || row < 0 || row >= numRows);
}

PVector toMatrixPos(int pixelX, int pixelY) {
  // overlay a square grid
  float a =  cos(radians(30)) * tileSize;
  float g =  sin(radians(30)) * tileSize;
  float cellWidth = 2 * a;
  float cellHeight = tileSize * 3 / 2;
  int col = floor(pixelX / cellWidth);
  int row = floor(pixelY / cellHeight);
  float xInCell = pixelX - col * cellWidth;
  float yInCell = pixelY - row * cellHeight;
  /*println("cellWidth: " + cellWidth + ", cellHeight: " + cellHeight +
          ", col: " + col + ", row: " + row +
          ", xInCell: " + xInCell + ", yInCell: " + yInCell);*/
  
  float m = g / a;
  if(row % 2 == 0) {
    // println("roof-cell");
    if(yInCell < xInCell * -m + g)
      return new PVector(col - 1, row - 1);
    if(yInCell < xInCell * m - g)
      return new PVector(col, row - 1);
    return new PVector(col, row);
  } else {
    // println("y-cell");
    if(xInCell < a) {
      if(yInCell < xInCell * m)
        return new PVector(col, row - 1);
      return new PVector(col - 1, row);
    } else {
      if(yInCell < xInCell * -m + 2 * g)
        return new PVector(col, row - 1);
      return new PVector(col, row);
    }
  }
}

void draw() {
  
  fill(200);
  rect(0, 0, width, height);
  
  if(!hitboxesEnabled)
    showTiles(); // shown by default
  if(hitboxesEnabled)
    showHitboxes();
  if(gridEnabled)
    showGrid();
  
  fill(0);
  textSize(20);
  text("g: toggle grid, h: hitboxes", 0, height);
  
}

void showTiles() {
  for(int row = 0; row < map.length; row++) {
    for(int col = 0; col < map[row].length; col++) {
      boolean isSelected = (col == selX && row == selY);
      map[row][col].show(col, row, isSelected);
    }
  }
}

void showGrid() {
  float a =  cos(radians(30)) * tileSize;
  float g =  sin(radians(30)) * tileSize;
  float cellWidth =  2 * a;
  float cellHeight = tileSize * 3 / 2;
  for(float x = 0; x < width; x += cellWidth) {
    for(float y = 0; y < height; y += cellHeight) {
      noFill();
      stroke(100);
      rect(x, y, cellWidth, cellHeight);
    }
  }
}

void showHitboxes() {
  loadPixels();
  for(int x = 0; x < width; x++) {
    for(int y = 0; y < height; y++) {
      PVector pos = toMatrixPos(x, y);
      int col = (int)pos.x;
      int row = (int)pos.y;
      if(!isOutOfBounds(col, row)) {
        color c;
        if(col % 2 == 0) {
          if(row % 2 == 0)
            c = color(255, 0, 0);
          else
            c = color(0, 0, 255);
        } else {
          if(row % 2 == 0)
            c = color(0, 255, 0);
          else
            c = color(255, 255, 0);
        }
        int index = y * width + x;
        pixels[index] = c;
      }
    }
  }
  updatePixels();
  if(!isOutOfBounds(selX, selY))
    map[selY][selX].show(selX, selY, true);
}
