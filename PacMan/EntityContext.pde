public interface EntityContext {

  boolean isSpace(Entity e, int col, int row);

  boolean isTileAt(int col, int row);

  int getTileSize();
}
