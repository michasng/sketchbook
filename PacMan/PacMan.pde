public static final int FPS = 30, MOVE_DELAY = 10;

private int moveTimer;
private int xOffset, yOffset;

private TileMap map;
private Entity player;

void setup() {
  fullScreen();
  frameRate(FPS);

  int numCols = 32;
  int numRows = 32;
  int tileSize = width / numCols;
  if(tileSize > height / numRows)
    tileSize = height / numRows;
  
  map = new TileMap(numCols, numRows, tileSize);
  map.generate();

  player = new Player(numCols / 2, numRows / 2, 2, 2, map);

  xOffset = width / 2 - numCols / 2 * tileSize;
  yOffset = height / 2 - numRows / 2 * tileSize;
}



void draw() {
  moveTimer++;
  if (moveTimer > MOVE_DELAY) {
    moveTimer = 0;
    if (mousePressed) {
      int clickCol = (pmouseX - xOffset) / map.getTileSize();
      int clickRow = (pmouseY - yOffset) / map.getTileSize();
      player.goTo(clickCol, clickRow);
    } else
      player.updateOldPos();
  }

  background(0);
  map.show(xOffset, yOffset);
  player.show(xOffset, yOffset, (float) moveTimer / MOVE_DELAY);
}
