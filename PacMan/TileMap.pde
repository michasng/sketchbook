public class TileMap implements EntityContext {

  private int numCols, numRows, tileSize;

  private boolean[][] tiles;

  public TileMap(int numCols, int numRows, int tileSize) {
    this.numCols = numCols;
    this.numRows = numRows;
    this.tileSize = tileSize;

    tiles = new boolean[numRows][numCols];
  }

  public void generate() {
    for (int row = 0; row < numRows; row++) {
      for (int col = 0; col < numCols; col++) {
        if (col == 0 || row == 0 || col == numCols - 1 || row == numRows - 1 || random(1) < 0.1)
          tiles[row][col] = true;
      }
    }
  }

  public void show(int xOffset, int yOffset) {
    for (int row = 0; row < numRows; row++) {
      for (int col = 0; col < numCols; col++) {
        if (tiles[row][col])
          fill(#0000ff);
        else
          fill(0);
        rect(col * tileSize + xOffset, row * tileSize + yOffset, 
          tileSize, tileSize);
      }
    }
  }

  public void setTile(boolean tile, int col, int row) {
    this.tiles[row][col] = tile;
  }

  public boolean isSpace(Entity e, int destCol, int destRow) {
    for (int col = destCol; col < destCol + e.getColSize(); col++) {
      for (int row = destRow; row < destRow + e.getRowSize(); row++) {
        if (isInBounds(col, row) && isTileAt(col, row))
          return false;
      }
    }
    return true;
  }

  public boolean isTileAt(int col, int row) {
    return tiles[row][col];
  }

  public boolean isInBounds(int col, int row) {
    return col >= 0 && col < numCols && row >= 0 && row < numRows;
  }

  public int getTileSize() {
    return tileSize;
  }
}
