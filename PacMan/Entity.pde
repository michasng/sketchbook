public abstract class Entity {
  
  protected int col, row, colSize, rowSize;
  protected int oldCol, oldRow;
  protected EntityContext context;
  
  public Entity(int col, int row, int colSize, int rowSize, EntityContext context) {
    this.col = col;
    this.row = row;
    updateOldPos();
    this.colSize = colSize;
    this.rowSize = rowSize;
    this.context = context;
  }
  
  public void goTo(int targetCol, int targetRow) {
    updateOldPos();
    
    int eCenterCol = col + colSize / 2;
    int eCenterRow = row + rowSize / 2;
      
    int colDiff = abs(targetCol - eCenterCol);
    int rowDiff = abs(targetRow - eCenterRow);
    
    int xMove = Integer.signum(targetCol - eCenterCol);
    int yMove = Integer.signum(targetRow - eCenterRow);
    
    if(context.isSpace(this, col + xMove, row + yMove)) {
      col += xMove;
      row += yMove;
    } else if(context.isSpace(this, col, row + yMove))
    row += yMove;
    else if(context.isSpace(this, col + xMove, row))
    col += xMove;
    
  }
  
  public abstract void show(int xOffset, int yOffset, float interpolation);
  
  public void updateOldPos() {
    oldCol = col;
    oldRow = row;
  }
  
  public int getCol() {
    return col;
  }
  
  public int getX() {
    return col * context.getTileSize();
  }
  
  public int getRow() {
    return row;
  }
  
  public int getY() {
    return row * context.getTileSize();
  }
  
  public int getColSize() {
    return colSize;
  }
  
  public int getWidth() {
    return colSize * context.getTileSize();
  }
  
  public int getRowSize() {
    return rowSize;
  }
  
  public int getHeight() {
    return rowSize * context.getTileSize();
  }
  
}