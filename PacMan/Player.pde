public class Player extends Entity {
  
  public Player(int col, int row, int colSize, int rowSize, EntityContext context) {
    super(col, row, colSize, rowSize, context);
  }

  public void show(int xOffset, int yOffset, float interpolation) {
    fill(color(200, 200, 0));
    ellipseMode(CORNER);
    ellipse((oldCol + (col - oldCol) * interpolation) * context.getTileSize() + xOffset, 
      (oldRow + (row - oldRow) * interpolation) * context.getTileSize() + yOffset, 
      getWidth(), getHeight());
  }
}
