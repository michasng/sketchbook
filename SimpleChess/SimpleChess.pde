
static final int EMPTY = 0, PAWN = 1, BISHOP = 2, ROOK = 3, KNIGHT = 4, QUEEN = 5, KING = 6;

int[][] grid;
int numCols, numRows, tileSize;
int xOffset, yOffset;

int selCol, selRow;

ArrayList<PImage>[] images;

void setup() {
  size(640, 480);
  numRows = 8;
  numCols = 8;
  grid = new int[numRows][numCols];
  int maxTileHeight = height / grid.length;
  int maxTileWidth = width / grid[0].length;
  tileSize = (maxTileWidth < maxTileHeight) ? maxTileWidth : maxTileHeight;
  xOffset = (width - (numCols * tileSize)) / 2;
  yOffset = (height - (numRows * tileSize)) / 2;
  
  selCol = selRow = -1;
  
  setFigures();
  loadFigureImages();
}

void setFigures() {
  grid[0][0] = ROOK;
  grid[0][7] = ROOK;
  for(int col = 0; col < grid[1].length; col++)
    grid[1][col] = PAWN;
}

void loadFigureImages() {
  images = new ArrayList[2];
  String[] types = {"pawn", "bishop", "rook", "knight", "queen", "king"};
  images[0] = new ArrayList<PImage>();
  for(String type: types)
    images[0].add(loadImage("" + type + "_b.png"));
  images[1] = new ArrayList<PImage>();
  for(String type: types)
    images[1].add(loadImage("" + type + "_w.png"));
}

boolean isValid(int col, int row) {
  return(col >= 0 && col < numCols &&
    row >= 0 && row < numRows);
}

void mouseClicked() {
  int oldSelCol = selCol;
  int oldSelRow = selRow;
  selCol = (mouseX - xOffset) / tileSize;
  selRow = (mouseY - yOffset) / tileSize;
  if(selCol == oldSelCol && selRow == oldSelRow)
    selCol = selRow = -1;  
  
  if(isValid(oldSelCol, oldSelRow) && grid[oldSelRow][oldSelCol] != 0 &&
    isValid(selCol, selRow) && grid[selRow][selCol] == 0) {
    grid[selRow][selCol] = grid[oldSelRow][oldSelCol];
    grid[oldSelRow][oldSelCol] = 0;
    selCol = selRow = -1;
  }
}

void draw() {
  for(int row = 0; row < grid.length; row++) {
    for(int col = 0; col < grid[row].length; col++) {
      int x = col * tileSize + xOffset;
      int y = row * tileSize + yOffset;
      noStroke();
      if((row + col) % 2 == 0)
        fill(230, 230, 250);
       else
        fill(60, 60, 70);
      rect(x, y, tileSize, tileSize);
      int margin = tileSize / 8;
      if(grid[row][col] != EMPTY)
        image(getFigureImage(col, row), x + margin, y + margin, tileSize - 2*margin, tileSize - 2*margin);
    }
  }
  if(selCol < 0 || selRow < 0)
    return;
  fill(0, 0, 255, 100);
  rect(selCol * tileSize + xOffset, selRow * tileSize + yOffset, tileSize, tileSize);
}

PImage getFigureImage(int col, int row) {
  int type = grid[row][col];
  if(type < 0)
    return images[0].get(Math.abs(type));
  if(type > 0)
    return images[1].get(type);
  throw new Error("cannot get Image for an empty tile");
}