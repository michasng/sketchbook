import java.util.LinkedList;

public class SnakeEntity {

  private LinkedList<PVector> points;

  public SnakeEntity(int startX, int startY) {
    points = new LinkedList<PVector>();
    points.push(new PVector(numCols / 2, numRows / 2));
  }

  public void move(int moveX, int moveY) {
    if (moveX == 0 && moveY == 0)
      return;
    currentMoveDir = new PVector(moveX, moveY);

    PVector oldTop = points.peekFirst();
    PVector newTop = oldTop.copy().add(moveX, moveY);
    if (food.equals(newTop))
      eatFood();
    else if (outOfBounds(newTop) || grid[(int)newTop.y][(int)newTop.x] == true) {
      gameOver = true;
      return;
    } else { // only remove last, when no food was eaten
      PVector last = points.removeLast();  
      grid[(int)last.y][(int)last.x] = false;
    }
    points.addFirst(newTop);
    grid[(int)newTop.y][(int)newTop.x] = true;
  }
}