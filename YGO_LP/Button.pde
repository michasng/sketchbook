abstract class Button {

  int x, y, w, h;
  Runnable action;
  boolean clicked;

  Button(int x, int y, int w, int h, Runnable action) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.action = action;
  }

  boolean isInBounds(int x2, int y2) {
    return x2 >= x && x2 < x + w && y2 >= y && y2 < y + h;
  }

  void update() {
    boolean newClicked = mousePressed && isInBounds(mouseX, mouseY);
    if (!clicked && newClicked)
      action.run();
    clicked = newClicked;
  }

  abstract void show();
}
