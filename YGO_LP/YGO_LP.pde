LpPanel lpLeft, lpRight;

void setup() {
  fullScreen();
 
 lpLeft = new LpPanel(8000, 0, height / 2, width / 3);
 lpRight = new LpPanel(8000, width / 2, height / 2, width / 3);
}

void draw() {
  background(0);
  
  lpLeft.show();
  lpRight.show();
}