class UpButton extends Button {


  UpButton(int x, int y, int w, int h, Runnable action) {
    super(x, y, w, h, action);
  }

  void show() {
    if (clicked)
      fill(#B1000D);
    else fill(#D2D2D2); 
    triangle(x + w / 2, y, x, y + h, x + w, y + h);
  }
}
