class TextButton extends Button {
  
  String text;
  
  TextButton(int x, int y, int w , int h, Runnable action, String text) {
    super(x, y , w, h, action);
    this.text = text;
  }
  
  void show() {
    if (clicked)
      fill(#B1000D);
    else fill(#2F2F2F);
    rect(x, y, w, h);

    fill(0);
    textSize(h / 2);
    textAlign(CENTER, CENTER);
    text(text, x + w / 2, y + h / 2);
  }
}