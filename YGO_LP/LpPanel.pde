class LpPanel {

  int lp;
  Button[] up, down;
  int digits;
  String zeroChain;

  int x, y, w, h;
  int fontSize, distance;

  LpPanel(int startLp, int x, int y, int w) {
    this.lp = startLp;
    digits = 5;
    zeroChain = "";
    for(int i = 0; i < digits; i++) {
      zeroChain += '0';
    }
    
    this.x = x;
    this.w = w;
    distance = w / 5;
    fontSize = 2 * distance;
    this.h = 2 * fontSize;
    this.y = y - h / 2;

    up = new Button[digits];
    for (int i = 0; i < up.length; i++) {
      final int digit = up.length - i - 1;
      up[i] = new UpButton(x + i * distance, this.y, w / up.length, distance, new Runnable() {
          public void run() {
            lp += Math.pow(10, digit);
          }
        }
      );
    }
    down = new Button[digits];
    for (int i = 0; i < down.length; i++) {
      final int digit = down.length - i - 1;
      down[i] = new DownButton(x + i * distance, this.y + h - distance, w / down.length, distance, new Runnable() {
          public void run() {
            lp -= Math.pow(10, digit);
          }
        }
      );
    }
  }

  void show() {
    for (Button b : up) {
      b.update();
      b.show();
    }
    for (Button b : down) {
      b.update();
      b.show();
    } 

    fill(255);
    textSize(fontSize);
    textAlign(CENTER, CENTER);
    String str = Integer.toString(lp);
    str = (zeroChain + str);
    str = str.substring(str.length() - digits, str.length());
    for (int i = 0; i < str.length(); i++)
      text(str.charAt(i), x + (i+0.5) * distance, y + 2 * distance);
  }
}
