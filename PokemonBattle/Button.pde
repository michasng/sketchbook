public class Button {

  String text;
  int x, y, w, h;
  
  boolean hoveredOver, pressed;
  
  public Button(String text, int x, int y, int w, int h) {
    this.text = text;
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }
  
  public void checkMouse() {
    hoveredOver = (mouseX >= x && mouseY >= y && mouseX < x + w && mouseY < y + h);
      
    // has been released but is no longer pressed
    if(pressed && !mousePressed && hoveredOver)
      println("clicked " + text);
    
    pressed = hoveredOver && mousePressed;
  }
  
  public void show() {
    checkMouse();
    
    stroke(0);
    if(pressed)
      fill(color(255, 0, 0));
    else if(hoveredOver)
      fill(color(0, 255, 0));
    else fill(200);
    rect(x, y, w, h);
    
    fill(0);
    textAlign(CENTER, CENTER);
    textSize(h / 4);
    text(text, x + w / 2, y + h / 2);
  }
  
}