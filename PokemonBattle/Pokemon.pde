public class Pokemon {

  String name;
  int level, maxHp, hp, speed;
  // strength, defense, type(s), posoned, bruning, paralysed, sleeping, ...
  Attack[] attacks;
  
  public Pokemon(String name, int level, int maxHp, int speed, Attack... attacks) {
    this.name = name;
    this.level = level;
    this.maxHp = maxHp;
    this.speed = speed;
    heal();
    if(attacks.length > 4)
      throw new Error("Too many attacks");
    this.attacks = attacks;
  }
  
  public void heal() {
    hp = maxHp;
  }
  
}