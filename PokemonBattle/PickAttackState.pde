public class PickAttackState implements State {

  Pokemon self, opponent;
  
  Attack[] attacks;
  Button[] buttons;
  
  public PickAttackState(Pokemon self, Pokemon opponent) {
    this.attacks = self.attacks;
    
    buttons = new Button[4];
    int border = width / 32;
    int bWidth = width / 2 - 2 * border;
    int bHeight = height / 4 - 2 * border;
    buttons[0] = new Button("", border, height / 2 + border, bWidth, bHeight);
    buttons[1] = new Button("", border + width / 2, height / 2 + border, bWidth, bHeight);
    buttons[2] = new Button("", border, height / 2 + border + height / 4, bWidth, bHeight);
    buttons[3] = new Button("", border + width / 2, height / 2 + border + height / 4, bWidth, bHeight);
    
    for(int i = 0; i < attacks.length; i++) {
      buttons[i].text = attacks[i].name;
    }
  }
  
  @Override
  public void show() {
    fill(color(200, 50, 0));
    noStroke();
    rect(0, 0, width, height);
    
    for(Button button : buttons)
      button.show();
}

}