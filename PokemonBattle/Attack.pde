public class Attack {
  
  String name;
  int strength;
  
  public Attack(String name, int strength) {
    this.name = name; 
    this.strength = strength;
  }
  
}