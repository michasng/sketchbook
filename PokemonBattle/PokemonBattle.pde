
State state;
Pokemon self, opponent;

void setup() {
  size(480, 640);
  
  Attack[] attacks = new Attack[]{
    new Attack("Kratzer", 10),
    new Attack("Tackle", 5)
  };
  self = new Pokemon("Glumanda", 5, 60, 100, attacks);
  opponent = new Pokemon("Schiggy", 5, 60, 100, attacks);

  state = new PickAttackState(self, opponent);
}

void draw() {
  background(0);  
  state.show();
}