public class Paddle extends Entity {

  private int w, h;

  // auto center vertically
  public Paddle(float x, int w, int h) {
    super(x, height / 2 - h/2, 0.5);
    this.w = w;
    this.h = h;
    
  }

  @Override
    public void calcNextPos() {
    super.calcNextPos();
    if (nextPos.y < 0)
      nextPos.y = 0;
    if (nextPos.y + h >= height)
      nextPos.y = height - h;
  }
  
  public void collide(Puck puck) {
    if (Pong.isColliding(nextPos, w, h, puck.pos, puck.radius))
      puck.deflect(this);
  }

  @Override
    public void show() {
    rect(pos.x, pos.y, w, h);
  }
}