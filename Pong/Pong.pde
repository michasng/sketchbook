private IntList keyList;

private Puck puck;
private Paddle leftPaddle, rightPaddle;

private int scoreLeft, scoreRight;

public void setup() {
  size(1024, 768);
  // fullScreen();
  frame.toFront();
  frame.requestFocus(); // window accepts key events right away
  frameRate(30);

  keyList = new IntList();

  int radius = height / 32;
  puck = new Puck(width / 2, height / 2, radius);
  leftPaddle = new Paddle(radius, radius, radius * 4);
  rightPaddle = new Paddle(width - 2 * radius, radius, radius * 4);
}

public void draw() {
  if (keyList.size() > 0)
    onKeyPressed();

  puck.update();
  puck.collide(leftPaddle, rightPaddle);
  puck.move();

  leftPaddle.update();
  leftPaddle.collide(puck);
  leftPaddle.move();

  rightPaddle.update();
  rightPaddle.collide(puck);
  rightPaddle.move();

  checkGoals();

  // ----- drawing -----

  background(0);

  puck.show();
  leftPaddle.show();
  rightPaddle.show();

  textSize(height / 10);
  textAlign(RIGHT, TOP);
  text(scoreLeft + ":", width / 2, 0);
  textAlign(LEFT, TOP);
  text(scoreRight, width / 2, 0);
}

public void checkGoals() {
  if (puck.pos.x - puck.radius <= 0) {
    scoreRight++;
    puck.reset();
  } else if (puck.pos.x + puck.radius > width) {
    scoreLeft++;
    puck.reset();
  }
}

public void onKeyPressed() {
  int paddleAcc = height / 8;

  for (int currentKey : keyList)
    switch(currentKey) {
    case 'w':
      leftPaddle.applyForce(0, -paddleAcc);
      break;
    case 's':
      leftPaddle.applyForce(0, paddleAcc);
      break;
    case UP:
      rightPaddle.applyForce(0, -paddleAcc);
      break;
    case DOWN:
      rightPaddle.applyForce(0, paddleAcc);
      break;
    case ' ':
      puck.reset();
      break;
    }
}

public void keyPressed() {
  keyList.append(key == CODED ? keyCode: key);
}

public void keyReleased() {
  keyList.removeValues(key == CODED ? keyCode : key);
}

// https://stackoverflow.com/questions/401847/circle-rectangle-collision-detection-intersection
public static boolean isColliding(PVector recPos, int recW, int recH, PVector cirPos, int radius) {
  PVector recCenter = new PVector(recPos.x + recW / 2, recPos.y + recH / 2);
  float xDist = abs(cirPos.x - recCenter.x);
  float yDist = abs(cirPos.y - recCenter.y);

  if (xDist > (recW/2 + radius)) { 
    return false;
  }
  if (yDist > (recH/2 + radius)) { 
    return false;
  }

  if (xDist <= (recW/2)) { 
    return true;
  } 
  if (yDist <= (recH/2)) { 
    return true;
  }

  double cornerDistance_sq = Math.pow(xDist - recW/2, 2) + Math.pow(yDist - recH/2, 2);

  return (cornerDistance_sq <= Math.pow(radius, 2));
}