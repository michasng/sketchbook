public class Puck extends Entity {

  private int radius;
  private int startX, startY;
  private float startSpeed, speedUp;

  // auto center
  public Puck(int x, int y, int radius) {
    super(x, y, 1);
    this.startX = x;
    this.startY = y;
    this.radius = radius;
    this.startSpeed = radius / 3f;
    this.speedUp = radius / 64f; // float for precision (might be 0 as int)
  }

  public void reset() {
    pos = new PVector(startX, startY);
    vel = PVector.random2D().mult(startSpeed);
  }

  @Override
    public void calcNextPos() {
    super.calcNextPos();

    if (nextPos.y-radius <= 0 || nextPos.y+radius > height)
      vel.y *= -1;

    super.calcNextPos();
  }

  public void collide(Paddle... paddles) {
    for (Paddle paddle : paddles)
      if (Pong.isColliding(paddle.pos, paddle.w, paddle.h, nextPos, radius)) {
        deflect(paddle);
      }
  }

  public void deflect(Paddle paddle) {
    PVector paddleCenter = new PVector(paddle.pos.x + paddle.w / 2, paddle.pos.y + paddle.h / 2);
    PVector dirToPuck = new PVector(pos.x, pos.y).sub(paddleCenter).normalize();
    vel = dirToPuck.mult(vel.mag() + speedUp); // the puck speeds up when deflecting from a paddle
  }

  @Override
    public void show() {
    ellipseMode(CENTER);
    ellipse(pos.x, pos.y, 2 * radius, 2 * radius);
  }
}