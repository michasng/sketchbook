public abstract class Entity {

  protected PVector pos, nextPos, vel, acc;
  protected int maxVel; // limits the velocity
  protected float friction; // slows the entity down over time

  public Entity(float x, float y, float friction) {
    this.pos = new PVector(x, y);
    this.vel = new PVector();
    this.acc = new PVector();
    this.maxVel = height / 36;
    this.friction = friction; // 1 is no friction, < 1 slows down, > 1 speeds up
  }

  public void update() {
    vel.add(acc);
    vel.limit(maxVel);
    acc.mult(0);
    vel.mult(friction);
    calcNextPos();
  }

  public void calcNextPos() {
    nextPos = new PVector(pos.x, pos.y);
    nextPos.add(vel);
  }

  public void move() {
    pos = nextPos;
  }

  public void applyForce(int xForce, int yForce) {
    applyForce(new PVector(xForce, yForce));
  }

  public void applyForce(PVector force) {
    this.acc.add(force);
  }

  public abstract void show();
}